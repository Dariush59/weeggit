<?hh //strict
namespace app
{
    use nutsNBolts\NutsNBolts;
    use nutshell\exception\NutshellException;
    use nutshell\plugin\database\datasource\manager\Manager as DataSourceManager;
    use nutshell\plugin\database\orm\ORM;
    use nutshell\plugin\debug\debug\Debug;
    use nutshell\plugin\application\router\Router;
    use nutshell\plugin\application\application\Application;
    

    class App extends Application
    {
        public function run():void
        {

        }
    }
}